###Passos para iniciar no bitbucket
1. No CMD digite ssh-keygen e dê enter até terminar as condições
2. Entre na pasta "C:/Users/(usuário)/.ssh"
3. Copie os dados do arquivo id_rsa.pub
4. Entre no seu perfil e em seguida nas configurações, na página de configurações entre na opção SSH keys
5. Clique em Add Key
5. Cole os dados pegos do arquivo id.rsa.pub
6. Adicione do campo "Label" " PC 'Seu Nome' "
7. Finalize clicando em Add Key
8. Clique em clonar e na pequena janela no canto superior direito clique em https e mude para ssh e copie o comando
9. Crie a seguinte pasta “C:/dev” com "dev" sendo todo minúsculo
10.	No CMD digite cd c:/dev
11. Em seguida cole o comando que você copiou no passo 8 e de enter
12. Inicie os passos para iniciar a aplicação
#
###Siga os passos para iniciar a aplicação

1.	Baixe o Python 3
2.	Instale o Python 3

#
###Iniciar a aplicação pelo Git bash caso não esteja usando Windows
1.	Baixe o Git Bash por esse link (https://git-scm.com/download)
2.	Instale o Git Bash
3.	E digite no git bash os seguintes comandos
    a.	cd c:/dev/project2
    b.	source venv/scripts/activate
    c.	python main.py
#
###Iniciar a aplicação pelo CMD do Windows
1.	Digite os seguintes comandos
    a.	cd c:/dev/project2
    b.	c:/dev/project2/venv/scripts/activate
    c.	python main.py
