print("APLICAÇÃO PARA LEITURA DE EXCEL INICIADO v1")

import os

import pandas as pd

x = 0
# y = 0

# Load in the workbook
file_name = 'C:\\Users\\%s\\Documents\\test.xlsx' % (os.getlogin())
file = 'C:\\Users\\%s\\Documents' % (os.getlogin())
xl_file = pd.ExcelFile(file_name)

dfs = pd.read_excel(file_name, sheet_name='Planilha1')

df = pd.DataFrame(dfs, columns=['Produto', 'Tipo Contrato', 'Data/Hora', 'MWm', 'MWh', 'Preço (R$)', 'Cancelado'])

df['Dia'] = pd.to_datetime(df['Data/Hora'], format='%d/%m/%Y %H:%M:%S').dt.strftime('%d/%m/%Y')

df['Semana Ano'] = pd.to_datetime(df['Data/Hora'], format='%d/%m/%Y %H:%M:%S').dt.strftime('%V%Y')

df = df[df['Tipo Contrato'] != 'Boleta']

i = len(df['Produto'])

print("INICIADO")

while x < i:
    produto = df['Produto'].values[x]
    semanano = df['Semana Ano'].values[x]

    # while y < i:

    balcao = df[(df['Produto'] == produto) & (df['Tipo Contrato'] == 'Balcão') & (
            df['Semana Ano'] == semanano) & (df['Cancelado'] == 'Não')]

    abertura = df[(df['Produto'] == produto) & (df['Data/Hora'] <= balcao['Data/Hora'].min()) & (
            df['Tipo Contrato'] == 'Balcão') & (
                          df['Semana Ano'] == semanano) & (df['Cancelado'] == 'Não')]

    fechamento = df[(df['Produto'] == produto) & (df['Data/Hora'] >= balcao['Data/Hora'].max()) & (
            df['Tipo Contrato'] == 'Balcão') & (
                            df['Semana Ano'] == semanano) & (df['Cancelado'] == 'Não')]

    # Mínimo
    balcao['Mínimo%s' % x] = balcao['Preço (R$)'].min()

    # Mínimo
    df['Mínimo%s' % x] = balcao['Mínimo%s' % x]

    # Máximo
    balcao['Máximo%s' % x] = balcao['Preço (R$)'].max()

    # Máximo
    df['Máximo%s' % x] = balcao['Máximo%s' % x]

    # Abertura
    abertura['Abertura%s' % x] = abertura['Preço (R$)']

    # Abertura
    df['Abertura%s' % x] = abertura['Abertura%s' % x]

    # Fechamento
    fechamento['Fechamento%s' % x] = fechamento['Preço (R$)']

    # Fechamento
    df['Fechamento%s' % x] = fechamento['Fechamento%s' % x]

    # MWm
    balcao['Volume MWm%s' % x] = balcao['MWm']

    # MWm
    df['Volume MWm%s' % x] = balcao['Volume MWm%s' % x]

    # MWh
    balcao['Volume MWh%s' % x] = balcao['MWh']

    # MWh
    df['Volume MWh%s' % x] = balcao['Volume MWh%s' % x]

    # Quantidade
    balcao['Quantidade%s' % x] = balcao['Produto']

    # Quantidade
    df['Quant. Negociações%s' % x] = balcao['Quantidade%s' % x].str.count(produto).sum()

    df.loc[(df['Produto'] == produto) & (df['Semana Ano'] == semanano), 'Máximo'] = df['Máximo%s' % x]
    df.loc[(df['Produto'] == produto) & (df['Semana Ano'] == semanano), 'Mínimo'] = df['Mínimo%s' % x]

    df.loc[(df['Produto'] == produto) & (df['Semana Ano'] == semanano), 'Abertura'] = df['Abertura%s' % x]
    df.loc[(df['Produto'] == produto) & (df['Semana Ano'] == semanano), 'Fechamento'] = df['Fechamento%s' % x]

    df.loc[(df['Produto'] == produto) & (df['Semana Ano'] == semanano), 'Volume MWm'] = df['Volume MWm%s' % x].sum()
    df.loc[(df['Produto'] == produto) & (df['Semana Ano'] == semanano), 'Volume MWh'] = df['Volume MWh%s' % x].sum()

    df.loc[(df['Produto'] == produto) & (df['Semana Ano'] == semanano), 'Quant. Negociações'] = df[
        'Quant. Negociações%s' % x]

    del df['Máximo%s' % x]
    # del df['Máximo1%s' % x]

    del df['Mínimo%s' % x]
    # del df['Mínimo1%s' % x]

    del df['Abertura%s' % x]
    del df['Fechamento%s' % x]

    del df['Volume MWm%s' % x]
    del df['Volume MWh%s' % x]

    del df['Quant. Negociações%s' % x]

    print("Ação:")
    print(produto)
    print(x)
    # y = 0
    x = x + 1

export_excel = df.to_excel(r'%s\Negocios_Semanal_Final.xlsx' % (file), index=None, header=True)

styled = (df.style
            .applymap(lambda v: 'background-color: %s' % 'red' if v=='Sim' else ''))
styled.to_excel(r'%s\Negocios_Semanal_Final1.xlsx' % (file), engine='openpyxl')

print(df)
